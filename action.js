/**
 * Created by kystok on 11.01.18.
 */

function SendPost(ID,text) {
    return new Promise(function(resolve,reject){
        $.ajax({
            url: "addComment.php",
            method: "get",
            data: {'parentID':ID, 'text':text },
            dataType: "json",
            statusCode: {
                200: function(id) {
                    resolve(id);
                },
                403: function(ERROR) {
                    responce(0);
                    var error = JSON.parse(ERROR.responseText);
                    console.log(error)
                }
            }
        });
    });

}

function sendComment(event) {
    var el = event.target;
    var parentID = el.parentNode.parentNode.getAttribute("data-id");
    var text = el.parentNode.childNodes.item(0).value;
    sending(parentID,text)
        .then(function (id) {
            $("[data-id=" + parentID + "]").append(_create_ul(text, id));
        });
    el.parentNode.childNodes.item(1).value = "";
    el.parentNode.classList.remove("vis");
    el.parentNode.classList.add("hid");
}

function chklvl(el) {
    var parentEl = el.parentNode.parentNode.parentNode;
    var lvl=0;
    console.log(lvl,parentEl);
    console.log("////////");
    while (!parentEl.hasAttributes("id")){
        lvl++;
        parentEl = parentEl.parentNode.parentNode.parentNode;
    }
    return (lvl<5)? lvl : false;
}

function comment(event) {
    var el = event.target.parentNode.parentNode.childNodes.item(2);
    if (el.classList.contains("hid")) {
        el.classList.remove("hid");
        el.classList.add("vis");
    } else {
        el.classList.add("hid");
        el.classList.remove("vis");
    }
    const parentID = el.parentNode.parentNode.getAttribute("data-id");
    console.log("lvl ",chklvl(el));
}

function deleteComment(event) {
    const commentID = event.target.parentNode.parentNode.getAttribute("data-id");
    $.ajax({
        url: "delComment.php",
        method: "get",
        data: {'id':commentID},
        statusCode: {
            200: function(res) {
                if(res)
                {$("[data-id=" + commentID + "]").remove()}
                else {alert("Ошибка")}
            }
        }
    });
 }

function addComment() {
    var text = $("#textComment").val();
    $("#textComment").val("");
    sending(0, text)
        .then(function (id) {
            $("#list").append(_create_li(text, id));
        });

}

function sending(parentID, text) {
    return new Promise(function(resolve){
        SendPost(parentID, text)
            .then(function (id) {
                resolve(id);
            })
    })

}
window.onload = function () {
    loadAll();
};

function loadAll() {
    return new Promise(function(resolve,reject){
        $.ajax({
            url: "showAllComment.php",
            method: "get",
            data: {},
            statusCode: {
                200: function(list) {
                    var result = jQuery.parseJSON(list);
                    show(result);
                },
                403: function(ERROR) {
                    responce(0);
                    var error = JSON.parse(ERROR.responseText);
                    console.log(error)
                }
            }
        });
    });
}


function show(list) {
    list.forEach(function (item) {
        if (item.parentID == 0) {
            $("#list").append(_create_li(item.text, item.id))
        } else {
            $("[data-id=" + item.parentID + "]").append(_create_ul(item.text, item.id))
        }
    })
}


function _create_ul(text, id) {
    var ul = document.createElement("ul");
    ul.append(_create_li(text, id));
    return ul;
}

function _create_li(text, id) {
    var li = document.createElement("li");
    var div__comments_item = document.createElement("div");
    var div__comments_text = document.createElement("div");
    var span = document.createElement("span");
    var div__comments_item_control = document.createElement("div");
    var btn__comments_item_control = document.createElement("button");
    var btn_delete__comments_item_control = document.createElement("button");
    var div__comment_input = document.createElement("div");
    var textArea = document.createElement("textarea");
    var btn_comment_input = document.createElement("button");
    span.innerHTML = text;

    btn__comments_item_control.className = "btn";
    btn__comments_item_control.innerHTML = "Comment";

    btn_delete__comments_item_control.className = "btn-delete";
    btn_delete__comments_item_control.innerHTML = "Delete";

    textArea.type = "text";
    textArea.placeholder = "Enter comment";


    btn_comment_input.innerHTML = "Send";

    div__comments_text.className = "comments-text";
    div__comments_text.appendChild(span);
    div__comments_item_control.className = "comments-item-control";
    div__comments_item_control.appendChild(btn__comments_item_control);
    div__comments_item_control.appendChild(btn_delete__comments_item_control);
    //div__comment_input.appendChild(textArea);

    div__comment_input.className = "comment-input hid";
    div__comment_input.appendChild(textArea);
    div__comment_input.appendChild(btn_comment_input);

    div__comments_item.className = "comments-item";
    div__comments_item.setAttribute("data-id", id);
    div__comments_item.appendChild(div__comments_text);
    div__comments_item.appendChild(div__comments_item_control);
    div__comments_item.appendChild(div__comment_input);
    li.appendChild(div__comments_item);

    btn_comment_input.setAttribute("onclick", "sendComment(event)");
    btn_delete__comments_item_control.setAttribute("onclick", "deleteComment(event)");
    btn__comments_item_control.setAttribute("onclick", "comment(event)");

    return li;
}




